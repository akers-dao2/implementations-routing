﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Management.Automation;
using System.Text;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Net;
using System.Web.Http;

namespace PowerShellTest
{
    public class ImplementPowerShell
    {
        PowerShell ps;

        public ImplementPowerShell()
        {

        }

        public ImplementPowerShell(string mapPath, string appendPath)
        {
            ps = PowerShell.Create();

            //create string path to the app_data folder
            string path = HttpContext.Current.Server.MapPath(mapPath);
            //create string path to the database.ps1 powershell
            StringBuilder psPath = new StringBuilder(path).Append(appendPath);

            //create powershell command
            ps.AddCommand(psPath.ToString());
            //add parameters to the powershell command
            ps.AddParameter("url", path);

         
        }

        public void AddParameter(Dictionary<string,object> p)
        {
            ps.AddParameters(p);
        }

        public HttpResponseMessage invokePS(string search, ApiController api)
        {
            HttpResponseMessage respMsg;
            Collection<PSObject> results;
            

            try
            {
                //execute powershell command
                results = ps.Invoke();
            }
            catch (Exception e)
            {
                throw e;
            }

            // retrieve errors from the powershell
            Collection<ErrorRecord> errors = ps.Streams.Error.ReadAll();

            //list to store return results or errors
            List<object> list = new List<object>();

            try
            {
                if (errors.Count > 0)
                {
                    foreach (ErrorRecord item in errors)
                    {
                        list.Add(item.Exception.Message);
                    }
                    respMsg = api.Request.CreateResponse<List<object>>(HttpStatusCode.BadRequest, list);
                }
                else
                {
                    if (search.Length < 0)
                    {
                        list.Add("ok");
                    }
                    else
                    {
                        foreach (PSObject item in results)
                        {
                            Hashtable ht = (Hashtable)item.BaseObject;

                            list.Add(ht[search]);
                        }
                    }

                    respMsg = api.Request.CreateResponse<List<object>>(HttpStatusCode.Accepted, list);
                    
                }


            }
            catch (Exception e)
            {
                Object m = new { data = e.Message };
                respMsg = api.Request.CreateResponse<object>(HttpStatusCode.ServiceUnavailable, m);

            }


            return respMsg;
        }
    }
}