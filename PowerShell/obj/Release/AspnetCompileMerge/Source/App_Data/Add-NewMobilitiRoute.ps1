﻿<#
.Synopsis
   Short description
.DESCRIPTION
   Long description
.EXAMPLE
   Example of how to use this cmdlet
.EXAMPLE
   Another example of how to use this cmdlet
#>



    [CmdletBinding()]
    Param
    (
        # Param1 help description
        [Parameter(ValueFromPipelineByPropertyName=$true,
                   Position=0,HelpMessage="Enter the 3 letter acronym of the client")]
        [string]
        [ValidateLength(3,3)]
        $Acronym,
        [ValidateSet('remove','listall','add')]
        [string]
        $Options,
        [string]
        $url
    )

   
 
    Begin
    {
        
        $db = Invoke-Expression -Command '&$url\database.ps1 -options read -url $url'
        #create a backup of the web.config file before modifying it.
        Copy-Item -Path $db.webConfigLocation -Destination "$($db.webConfigLocation).bak" -Force
    }
    Process
    {      
    
     function CheckAcronym
    {
        if($Acronym.Length -le 0){
                Write-Error -Message "acronym is required" -Category InvalidArgument -RecommendedAction "enter acronym for adding to web.config file" -ErrorAction Stop
            }
    }
     
        Write-Verbose "Opening $($db.item('webConfigLocation'))"
        $webConfig = Get-Content -Path $db.webConfigLocation -Encoding String
        #First Item: <filter name="XXX40BANK_InHouse_Filter" filterType="XPath" filterData="//*[local-name() = 'AuthToken']/*[local-name() = 'OrganizationId'] = 'XXX40BANK'" />
        #Second Item: <add filterName="XXX40BANK_InHouse_Filter" endpointName="XXX40BANK_InHouseEndpoint" priority="3"/>
        #Third Item: <endpoint name="XXX40BANK_InHouseEndpoint" address="https://dnaweb.opensolutionsasp.com/MobilitiRouter/RoutingService.svc" binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding" contract="*"/>
        
        Write-Verbose "Opening $($db.webConfigLocationItems)"
        $val = Get-Content $db.webConfigLocationItems
        

        Write-Verbose "Uppercasing the acronym value"
        $upperCaseAcronym = $Acronym.ToUpper()

        switch ($Options)
        {
            'remove' {
                CheckAcronym
                $webConfig | Where-Object -FilterScript {$_ -notlike "*$($upperCaseAcronym)40BANK*"} | Out-File -FilePath $db.webConfigLocation

            }
            'listall' {
                $webConfig | Select-String -Pattern '[A-Z]{3}40BANK' | % {$_.Matches} | Select-Object -Property value -Unique
            
            }
            'add' {
                CheckAcronym
                
                $firstItem = $val[0].replace("XXX",$upperCaseAcronym)
                $secondItem = $val[1].replace("XXX",$upperCaseAcronym)
                $thirdItem = $val[2].replace("XXX",$upperCaseAcronym)

                $result = $webConfig | Select-String -Pattern "$($upperCaseAcronym)40BANK" -Quiet

				 if($result){
                        Write-Error -Message "The ORG ID $($Acronym)40BANK already exist in the $($db.webConfigLocation)" -Category ResourceExists -RecommendedAction "Open the $db.webConfigLocation remove the duplicate ORG ID" -ErrorAction Stop
        
                    }
                
                for ($i = 1; $i -lt 3; $i++)
                { 
                    $result = $webConfig | Select-String -Pattern "$($db.searchItems[$i])" -Quiet

                    if(!$result){
                        Write-Error -Message "The search item $($db.searchItems[$i]) cannot be found in $($db.webConfigLocation)" -Category ResourceExists -RecommendedAction "Open the $db.webConfigLocation remove the duplicate ORG ID" -ErrorAction Stop
        
                    }

                }
                
        
                Write-Verbose "Adding $firstItem to Route Filter"
                $webConfig = $webConfig.replace($db.searchItems[0],"$($db.searchItems[0])`r`n`t`t$firstItem")
                Write-Verbose "Adding $secondItem to Filter Filter"
                $webConfig = $webConfig.replace($db.searchItems[1],"$($db.searchItems[1])`r`n`t`t$secondItem")
                Write-Verbose "Adding $thirdItem to ENDPOINT Filter"
                $webConfig = $webConfig.replace($db.searchItems[2],"$($db.searchItems[2])`r`n`t`t$thirdItem")
                Write-Verbose "Updating $($db.webConfigLocation)"
                Set-Content -Path $db.webConfigLocation -Value $webConfig
            }
            
            
            Default {
                $webConfig | Select-String -Pattern '[A-Z]{3}40BANK' | % {$_.Matches} | Select-Object -Property value -Unique
            }
        }
        
        
       
        
 
    }
    End
    {
        
    }


# SIG # Begin signature block
# MIIFuQYJKoZIhvcNAQcCoIIFqjCCBaYCAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQUfwEFrnUIEtyKkqYTJgqW7A7T
# FC+gggNCMIIDPjCCAiqgAwIBAgIQzTRBq5+KsrBBMPcSIyRmajAJBgUrDgMCHQUA
# MCwxKjAoBgNVBAMTIVBvd2VyU2hlbGwgTG9jYWwgQ2VydGlmaWNhdGUgUm9vdDAe
# Fw0xNDA4MDcwMzIyMzdaFw0zOTEyMzEyMzU5NTlaMBoxGDAWBgNVBAMTD1Bvd2Vy
# U2hlbGwgVXNlcjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKLdgkx2
# o5INVyUaPZrlXZctrzUmJlSy1nwaK03v7cgFea44jmWG09nM6nQWlZ7Qc4jdGeQJ
# 90pIK31nJWWIPCt7As2oJgMe99EF3U29TvDoAZHWNHz9qjM2+wKjE+QI+KI8nhi/
# fYbj73x0oNW5Rc3HTJGQdmuQZUzZjkw+E+bSZB4qpRpwAhLz/6DNpXRU9EBXvrOX
# JdAr14JcG4vYFriTVoISJo3OtWhwxSuIwyaMiB8r3l1Oc3rw5IYYXT/+JEClqBVG
# hRboPGYD2oxSFQZLNyRjX6sEHX6bdpG/4NNaztdVKp8ZSd8HB762YLVMr/15ircN
# kV4cfRhKIy7aFo8CAwEAAaN2MHQwEwYDVR0lBAwwCgYIKwYBBQUHAwMwXQYDVR0B
# BFYwVIAQjhfDXwZdAYsUctKoQWLDp6EuMCwxKjAoBgNVBAMTIVBvd2VyU2hlbGwg
# TG9jYWwgQ2VydGlmaWNhdGUgUm9vdIIQOroflVuFWptJMq/cpP+C6TAJBgUrDgMC
# HQUAA4IBAQAG9fuE+QM6PG96XC3Kd5V8SaTMYU51NmWTXJgUCYHRuEH3h0AKn+pV
# Gh+LPGPBLgDTx0GcmTFBd6HuBBE9axwMrb6cvXQNlLJppDjuv7Cacr5+oxvV+5br
# OsrBsy3M5hKAV1moFmwD18CdlaE14bAZQ6YHNL6LXrYzsHcx23idg5SpOHaafGsH
# p3qiE2L2KFU3dcQ7vKncJXu+SzOKLAFV8H6Qm2xGOmwfZPELYiRlU0yqt1iczEhB
# cJxUlKuyrMWl2Ry79ZN1rAdxmAugVAGYFS1vaAKHt7t2LpDbIy9IEMEo4p89RG2O
# CKT4WkRiCzUX8OEAiPXr7l5PdX6vwUiqMYIB4TCCAd0CAQEwQDAsMSowKAYDVQQD
# EyFQb3dlclNoZWxsIExvY2FsIENlcnRpZmljYXRlIFJvb3QCEM00QaufirKwQTD3
# EiMkZmowCQYFKw4DAhoFAKB4MBgGCisGAQQBgjcCAQwxCjAIoAKAAKECgAAwGQYJ
# KoZIhvcNAQkDMQwGCisGAQQBgjcCAQQwHAYKKwYBBAGCNwIBCzEOMAwGCisGAQQB
# gjcCARUwIwYJKoZIhvcNAQkEMRYEFEVjX8A/uPCIZbACI+kblFNEk7lSMA0GCSqG
# SIb3DQEBAQUABIIBAFf8oMe4/GJlPg1E1z80ZYO80bAgb76URitBOyDdXMIgrbJ1
# P1BwJU/UdgWF8H27fEZUeKvi6ba3T/FlncRlpkYy+FwKtpCr7d4v2tsU38CwxwKA
# iKTpKEfmUWtrynDdSyg1nsPpoUxYgFqOrxBNLVOv4htKPqo3e9aZDGC3DMSMEnX+
# 3N3rGjrW2JflV2CUVc8KQLx1UizLx+n7aoxNDW5uRZjxzP/4NCso33mVM3LoQDgP
# AnxAEwxGz0JZfXBvNwh82JyitlGxW/MbGd2Z/m/uQdpCfi7IV4/Vpuu49Sf4z30E
# 6oMGcbTCLW60EIORwhAGf5aVlFC/uhGK8PYfTkY=
# SIG # End signature block
