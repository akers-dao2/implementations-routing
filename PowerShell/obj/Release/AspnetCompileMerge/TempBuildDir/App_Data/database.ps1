﻿<#
.Synopsis
   Short description
.DESCRIPTION
   Long description
.EXAMPLE
   Example of how to use this cmdlet
.EXAMPLE
   Another example of how to use this cmdlet
#>
    [CmdletBinding()]
    [OutputType([String])]
    Param
    (
        # Param1 help description
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        [ValidateSet('add','save','read','update','delete')]
        [string]
        $options,
        [string]
        $key,
        [object]
        $value,
        [string]
        $url
    )

    Begin
    {
        #check to see if the database file exist
        $path = "$($url)\db.xml"

        $result = Test-Path -LiteralPath $path
        
        if($result -eq $true){
            $db = Import-Clixml -Path $path
        } else{
            $db = @{}       
            $db.Add("webConfigLocation","C:\Users\fakers\Downloads\Web.config")
            $db.Add("webConfigLocationItems","$($url)\WebConfigItems.txt")
            $db.Add("searchItems",('<!-- Implementations Routing Filter -->','<!-- Implementations Filter rules -->','<!-- Implementations ENDPOINT rules -->'))           
            Export-Clixml -Path $path -InputObject $db -ErrorAction Stop
        }

    }
    Process
    {
        function save{
            Export-Clixml -Path $path -InputObject $db -ErrorAction Stop
        }

        switch ($options)
        {
            'save' {
                
            }
            'add'{
                if(!$key -or !$value){
                    Write-Error -Message "Key and Value is required" -Category InvalidArgument
                }
                $db.add($key, $value)
                Write-Output @{msg="Data has been saved"}
                save
            }
            'read' {
                Import-Clixml -Path $path
            }
            'update' {
                if(!$key -or !$value){
                    Write-Error -Message "Key and Value is required" -Category InvalidArgument
                }
                $db[$key] = $value
                Write-Output @{msg="Data has been updated"}
                save
            }
            'delete' {
                if(!$key){
                    Write-Error -Message "Key is required" -Category InvalidArgument
                }
                $db.remove($key)
                Write-Output @{msg="Key value $key has been removed"}
                save
            }
            Default {
                Import-Clixml -Path $path
            }
        }
    }
    End
    {
    }

# SIG # Begin signature block
# MIIFuQYJKoZIhvcNAQcCoIIFqjCCBaYCAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQUR34AlErx+eaTHICOjDBCnpNq
# xfugggNCMIIDPjCCAiqgAwIBAgIQzTRBq5+KsrBBMPcSIyRmajAJBgUrDgMCHQUA
# MCwxKjAoBgNVBAMTIVBvd2VyU2hlbGwgTG9jYWwgQ2VydGlmaWNhdGUgUm9vdDAe
# Fw0xNDA4MDcwMzIyMzdaFw0zOTEyMzEyMzU5NTlaMBoxGDAWBgNVBAMTD1Bvd2Vy
# U2hlbGwgVXNlcjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKLdgkx2
# o5INVyUaPZrlXZctrzUmJlSy1nwaK03v7cgFea44jmWG09nM6nQWlZ7Qc4jdGeQJ
# 90pIK31nJWWIPCt7As2oJgMe99EF3U29TvDoAZHWNHz9qjM2+wKjE+QI+KI8nhi/
# fYbj73x0oNW5Rc3HTJGQdmuQZUzZjkw+E+bSZB4qpRpwAhLz/6DNpXRU9EBXvrOX
# JdAr14JcG4vYFriTVoISJo3OtWhwxSuIwyaMiB8r3l1Oc3rw5IYYXT/+JEClqBVG
# hRboPGYD2oxSFQZLNyRjX6sEHX6bdpG/4NNaztdVKp8ZSd8HB762YLVMr/15ircN
# kV4cfRhKIy7aFo8CAwEAAaN2MHQwEwYDVR0lBAwwCgYIKwYBBQUHAwMwXQYDVR0B
# BFYwVIAQjhfDXwZdAYsUctKoQWLDp6EuMCwxKjAoBgNVBAMTIVBvd2VyU2hlbGwg
# TG9jYWwgQ2VydGlmaWNhdGUgUm9vdIIQOroflVuFWptJMq/cpP+C6TAJBgUrDgMC
# HQUAA4IBAQAG9fuE+QM6PG96XC3Kd5V8SaTMYU51NmWTXJgUCYHRuEH3h0AKn+pV
# Gh+LPGPBLgDTx0GcmTFBd6HuBBE9axwMrb6cvXQNlLJppDjuv7Cacr5+oxvV+5br
# OsrBsy3M5hKAV1moFmwD18CdlaE14bAZQ6YHNL6LXrYzsHcx23idg5SpOHaafGsH
# p3qiE2L2KFU3dcQ7vKncJXu+SzOKLAFV8H6Qm2xGOmwfZPELYiRlU0yqt1iczEhB
# cJxUlKuyrMWl2Ry79ZN1rAdxmAugVAGYFS1vaAKHt7t2LpDbIy9IEMEo4p89RG2O
# CKT4WkRiCzUX8OEAiPXr7l5PdX6vwUiqMYIB4TCCAd0CAQEwQDAsMSowKAYDVQQD
# EyFQb3dlclNoZWxsIExvY2FsIENlcnRpZmljYXRlIFJvb3QCEM00QaufirKwQTD3
# EiMkZmowCQYFKw4DAhoFAKB4MBgGCisGAQQBgjcCAQwxCjAIoAKAAKECgAAwGQYJ
# KoZIhvcNAQkDMQwGCisGAQQBgjcCAQQwHAYKKwYBBAGCNwIBCzEOMAwGCisGAQQB
# gjcCARUwIwYJKoZIhvcNAQkEMRYEFFiPFxjPai7hpvpkai40rjRb/wY2MA0GCSqG
# SIb3DQEBAQUABIIBAH8zCi2ZDQEFZf+OEHJpzVZ/+jFCeTEIeQk7tGaBGB+LoNjY
# XIeadgXfq0NsCoMFiYx4oGhOFQisIOoRpLdQjkOysZ5vynL2tjjTzfXTCDBucoqJ
# Qb80d4t1DG83ulmvu+u+twNGlRFKrbCpIZC9PgMD3Xh9HlCoCI14VRLu9wFMh0yg
# e7ymO3X7sUc3Iba7UMxSzZ11XpBGyZu5sNEFJea6USSYf1OR3iFoldJC8aIPyWXH
# vvsHTEfInJE5YJ7zWCxrubPNihJ4qN+5SCE3bS/wmrb0fOvh3rcFeDOtRGCXpSZB
# tbJsl3Y7Oo+J1HaIf4a0YLb0qHxSd1dW0oxVCTg=
# SIG # End signature block
