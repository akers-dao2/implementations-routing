﻿var WebConfigApp = angular.module('WebConfig', ['ngRoute', 'ngResource', 'ui.bootstrap','ngAnimate'])
.config(function ($routeProvider) {
    $routeProvider
     .when('/', {
         templateUrl: 'List.html',
         controller: 'WebConfigController'
     });
});

WebConfigApp.factory('WebAPI', function ($resource) {

    return $resource('api/powershell/:acronym', { acronym: '@acronym' });

});

WebConfigApp.factory('SQLAPI', function ($resource) {

    return $resource('api/sql', { TranslatedRoutingID: '@TranslatedRoutingID' });

})

WebConfigApp.factory('DBWebAPI', function ($resource) {

    return $resource('api/dbwebapi', { url: '@url' }, { 'query': { method: 'GET', isArray: true } });

});

WebConfigApp.factory('Refresh', ['WebAPI', function (WebAPI) {

    return function ($scope) {
        WebAPI.query(function (FIs) {
            $scope.ListOfFIs = FIs;
        }, function (error) {
            $scope.alerts.push({ type: "danger", msg: error.data[0] })
            console.error(error);
            $scope.ListOfFIs = [];
        });
    };
        
}]);

WebConfigApp.factory('storage', [function () {

    return {
        setItem: function(item){
            localStorage.setItem("config", item);
            },
        getItem: function(){
            return localStorage.getItem("config");
        }
    }


}]);

WebConfigApp.factory('msgAlert', ['$timeout', '$rootScope', function ($timeout, $rootScope) {

    return {
        clear: function clearAlerts() {
            $timeout(function () {
                $rootScope.alerts = [];
            }, 3000);
        }
    }


}]);

WebConfigApp.controller('WebConfigController', ['$scope', 'WebAPI', 'Refresh', '$rootScope', '$filter', '$timeout',
    '$modal', 'storage', 'msgAlert', 'SQLAPI', function ($scope, WebAPI, Refresh, $rootScope, $filter, $timeout, $modal, storage, msgAlert, SQLAPI) {
    $scope.isDelete = false;

    Refresh($scope);

    $rootScope.alerts = [];

    $scope.closeAlert = function (index) {
        if ($scope.alerts[index].type == "danger") {
            $scope.acronym = "";
            $scope.sql = false;
        }
        $scope.alerts.splice(index, 1);
    };

    $scope.Delete = function (fi) {
        var acronym = fi.substring(0, 3);
        $scope.alerts.push({ type: "info", msg: "Processing" });
        WebAPI.remove({ acronym: acronym }, function () {
            Refresh($scope);
            $scope.alerts = [];
            var msg = fi.concat(" has been removed from the web.config file");
            $scope.alerts.push({ type: "success", msg: msg });
            clearAlerts();
        }, function (error) {
            console.error(error);
        });
        SQLAPI.remove({ TranslatedRoutingID: acronym }, function () {
            $scope.alerts = [];
            $scope.alerts.push({ type: "success", msg: "Data has been removed from SQL" });
            clearAlerts();
        }, function (error) {
            console.error(error);
        });
    };

    $scope.Add = function () {
        if ($scope.WebConfigForm.$valid) {
            $scope.alerts.push({ type: "info", msg: "Processing" });

            WebAPI.save({ acronym: $scope.acronym }, function () {
                Refresh($scope);
                if ($scope.sql == true) {
                    SQLAPI.save({ TranslatedRoutingID: $scope.acronym }, function () {
                        console.log("success");
                        $scope.sql = false;
                    }, function (error) {
                        console.error(error);
                    })
                }
                

                var uppercase = $filter('uppercase')($scope.acronym);
                $scope.acronym = "";
                $scope.alerts = [];
                console.log(uppercase);
                $scope.alerts.push({ type: "success", msg:  uppercase.concat("40BANK has been added from the web.config file") });
                clearAlerts();
            }, function (error) {
                $scope.alerts = [];
                console.error(error.data.data);
                $scope.alerts.push({ type: "danger", msg: error.data.data })
            });
        } else {
            $scope.error = "error"
        }
    };


    $scope.$watch("acronym", function (o, n) {
        if ($scope.WebConfigForm.acronym.$error.maxlength) {
            $scope.error = "error"

        } else {
            $scope.error = "noerror"
        }
    });

    $rootScope.$on("configUpdated", function () {
        Refresh($scope);
    })

    function clearAlerts() {
        $timeout(function () {
            $scope.alerts = [];
        }, 3000);
    }
    
}]);

WebConfigApp.controller('ModalController', ['$scope', 'DBWebAPI', 'Refresh', '$rootScope', '$filter', '$timeout', '$modal',
    'storage', 'msgAlert', function ($scope, DBWebAPI, Refresh, $rootScope, $filter, $timeout, $modal, storage, msgAlert) {

     $scope.openModal = function () {
         var modalInstance =  $modal.open({
            templateUrl: 'Config.html',
            controller: 'ModalController'
         })

         modalInstance.opened.then(function () {
             DBWebAPI.query(function (url) {
                 $rootScope.config = url;
             });
         })
    }

   

    $scope.ok = function (config) {
        var v = encodeURIComponent(config);
        DBWebAPI.save({ url: v }, function () {
            $rootScope.alerts.push({ type: "success", msg: "config file updated" });
            msgAlert.clear();
            $rootScope.$emit("configUpdated");
        });
        $scope.$close()
    }

    $scope.cancel = function () {
        $scope.$dismiss();
    }

}]);

