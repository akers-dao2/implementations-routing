﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PowerShellTest
{
    public class SQLController : ApiController
    {
        ImplementPowerShell PSInit = new ImplementPowerShell("~/App_Data", @"\invoke-SQLScripts.ps1");
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            PSInit.invokePS("", this);
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]SQLScripts value)
        {
            Dictionary<string, object> param = new Dictionary<string, object>();

            foreach (string item in value.param.Keys)
            {
                param[item] = value.param[item];
            }

            //param.Add("isDelete", true);

            PSInit.AddParameter(param);
            PSInit.invokePS("", this);
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public HttpResponseMessage Delete(string id)
        {
            PSInit.AddParameter(new Dictionary<string, object>() { { "TranslatedRoutingID", id }, { "isDelete", true } });
            PSInit.invokePS("", this);
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }

    public class SQLScripts
    {
        public Dictionary<string, object> param = new Dictionary<string, object>();

        protected string pTranslatedRoutingID, pGatewayServicePath, pBusinessServicePath, pDataServicePath;
        protected int pRoutingID;

        public string TranslatedRoutingID
        {
            get
            {
                return this.pTranslatedRoutingID;
            }
            set
            {
                this.pTranslatedRoutingID = value;
                param.Add("TranslatedRoutingID", value);
            }
        }
        public string GatewayServicePath {
            get
            {
                return this.pGatewayServicePath;
            }
            set
            {
                this.pGatewayServicePath = value;
                param.Add("GatewayServicePath", value);
            }
        }
        public string BusinessServicePath {
            get
            {
                return this.pBusinessServicePath;
            }
            set
            {
                this.pDataServicePath = value;
                param.Add("BusinessServicePath", value);
            }
        }
        public string DataServicePath {
            get
            {
                return this.pDataServicePath;
            }
            set
            {
                this.pDataServicePath = value;
                param.Add("DataServicePath", value);
            }
        }
        public int RoutingID
        {
            get
            {
                return this.pRoutingID;
            }
            set
            {
                this.pRoutingID = value;
                param.Add("RoutingID", value);
            }
        }

    }
}