﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Management.Automation;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;

namespace PowerShellTest.Controllers
{
    public class DBWebAPIController : ApiController
    {
        ImplementPowerShell PSInit = new ImplementPowerShell("~/App_Data", @"\database.ps1");
        static PowerShell init()
        {
            PowerShell ps = PowerShell.Create();

            //create string path to the app_data folder
            string path = HttpContext.Current.Server.MapPath("~/App_Data");
            //create string path to the database.ps1 powershell
            StringBuilder psPath = new StringBuilder(path).Append(@"\database.ps1");

            //create powershell comman
            ps.AddCommand(psPath.ToString());
            //add parameters to the powershell command
            ps.AddParameter("url", path);

            return ps;
        }

        static HttpResponseMessage invokePS(Collection<PSObject> results, Collection<ErrorRecord> errors, string search,ApiController api)
        {
            HttpResponseMessage respMsg;

            //list to store return results or errors
            List<object> list = new List<object>();

            try
            {
                if (errors.Count > 0)
                {
                    foreach (ErrorRecord item in errors)
                    {
                        list.Add(item.Exception.Message);
                    }

                   respMsg = api.Request.CreateResponse(HttpStatusCode.InternalServerError, list);
                }
                else
                {
                    if (search.Length < 0)
                    {
                        list.Add("ok");
                    }
                    else
                    {
                        foreach (PSObject item in results)
                        {
                            Hashtable ht = (Hashtable)item.BaseObject;

                            list.Add(ht[search]);
                        }
                    }
                    


                    respMsg = api.Request.CreateResponse(HttpStatusCode.OK, list);
                }


            }
            catch (Exception e)
            {
                Object m = new { data = e.Message };
                respMsg = api.Request.CreateResponse(HttpStatusCode.ServiceUnavailable, m);

            }

            return respMsg;
        }

        // GET: api/DBWebAPI
        public HttpResponseMessage Get()
        {
            PSInit.AddParameter(new Dictionary<string, object>() { { "options", "read" } });
           
            return PSInit.invokePS("webConfigLocation",this);
        }

        // GET: api/DBWebAPI/5
        
        public HttpResponseMessage Get(string id)
        {
            PSInit.AddParameter(new Dictionary<string, object>() { { "options", "read" } });
            return PSInit.invokePS(id,this);
        }

        // POST: api/DBWebAPI
        public HttpResponseMessage Post([FromBody]PostValue value)
        {
            PSInit.AddParameter(new Dictionary<string,object>() { {"options", "update"}, {"key", "webConfigLocation"}, {"value", HttpUtility.UrlDecode(value.url)} });
            PSInit.invokePS("msg",this);
            return Request.CreateResponse(HttpStatusCode.Created);
        }
        
        // PUT: api/DBWebAPI/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/DBWebAPI/5
        public void Delete(int id)
        {
        }
    }

    public struct PostValue
    {
        public string url { get; set; }
    }
}
