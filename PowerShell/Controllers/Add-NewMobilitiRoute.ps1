﻿<#
.Synopsis
   Short description
.DESCRIPTION
   Long description
.EXAMPLE
   Example of how to use this cmdlet
.EXAMPLE
   Another example of how to use this cmdlet
#>



    [CmdletBinding()]
    Param
    (
        # Param1 help description
        [Parameter(ValueFromPipelineByPropertyName=$true,
                   Position=0,HelpMessage="Enter the 3 letter acronym of the client")]
        [string]
        [ValidateLength(3,3)]
        $Acronym,
        [ValidateSet('remove','listall','add')]
        [string]
        $Options
    )

   
 
    Begin
    {
        $db = .\database.ps1 -options read 
        #create a backup of the web.config file before modifying it.
        Copy-Item -Path $db.webConfigLocation -Destination "$($db.webConfigLocation).bak" -Force
    }
    Process
    {      
    
     function CheckAcronym
    {
        if($Acronym.Length -le 0){
                Write-Error -Message "acronym is required" -Category InvalidArgument -RecommendedAction "enter acronym for adding to web.config file" -ErrorAction Stop
            }
    }
     
        Write-Verbose "Opening $($db.item('webConfigLocation'))"
        $webConfig = Get-Content -Path $db.webConfigLocation -Encoding String
        #First Item: <filter name="XXX40BANK_InHouse_Filter" filterType="XPath" filterData="//*[local-name() = 'AuthToken']/*[local-name() = 'OrganizationId'] = 'XXX40BANK'" />
        #Second Item: <add filterName="XXX40BANK_InHouse_Filter" endpointName="XXX40BANK_InHouseEndpoint" priority="3"/>
        #Third Item: <endpoint name="XXX40BANK_InHouseEndpoint" address="https://dnaweb.opensolutionsasp.com/MobilitiRouter/RoutingService.svc" binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding" contract="*"/>
        
        Write-Verbose "Opening $($db.webConfigLocationItems)"
        $val = Get-Content $db.webConfigLocationItems
        

        Write-Verbose "Uppercasing the acronym value"
        $upperCaseAcronym = $Acronym.ToUpper()

        switch ($Options)
        {
            'remove' {
                CheckAcronym
                $webConfig | Where-Object -FilterScript {$_ -notlike "*$($upperCaseAcronym)40BANK*"} | Out-File -FilePath $db.webConfigLocation

            }
            'listall' {
                $webConfig | Select-String -Pattern '[A-Z]{3}40BANK' | % {$_.Matches} | Select-Object -Property value -Unique
            
            }
            'add' {
                CheckAcronym
                
                $firstItem = $val[0].replace("XXX",$upperCaseAcronym)
                $secondItem = $val[1].replace("XXX",$upperCaseAcronym)
                $thirdItem = $val[2].replace("XXX",$upperCaseAcronym)

                $webConfig | foreach -Process {
                    if($_.Contains("$($Acronym)40BANK")){
                        Write-Error -Message "The ORG ID $($Acronym)40BANK already exist in the $db.webConfigLocation" -Category ResourceExists -RecommendedAction "Open the $db.webConfigLocation remove the duplicate ORG ID" -ErrorAction Stop
        
                    }
                }
        
        
                Write-Verbose "Adding $firstItem to Route Filter"
                $webConfig = $webConfig.replace($db.searchItems[0],"$($db.searchItems[0])`r`n`t`t$firstItem")
                Write-Verbose "Adding $secondItem to Filter Filter"
                $webConfig = $webConfig.replace($db.searchItems[1],"$($db.searchItems[1])`r`n`t`t$secondItem")
                Write-Verbose "Adding $thirdItem to ENDPOINT Filter"
                $webConfig = $webConfig.replace($db.searchItems[2],"$($db.searchItems[2])`r`n`t`t$thirdItem")
                Write-Verbose "Updating $($db.webConfigLocation)"
                Set-Content -Path $db.webConfigLocation -Value $webConfig
            }
            
            
            Default {}
        }
        
        
       
        
 
    }
    End
    {
        
    }

