﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Management.Automation;
using System.Collections;
using System.Runtime.Serialization.Json;
using System.Net.Http.Formatting;
using System.Text;
using System.Web;
using System.Collections.ObjectModel;


namespace PowerShellTest.Controllers
{
    public class PowerShellController : ApiController
    {
        //static PowerShell PShell()
        //{
        //    PowerShell ps = PowerShell.Create();
        //    string directory = HttpContext.Current.Server.MapPath("~/App_Data/Add-NewMobilitiRoute.ps1");
        //    ps.AddCommand(directory);

        //    return ps;
        //}
        // GET api/powershell
        public HttpResponseMessage Get()
        {

            HttpResponseMessage msg;

            string path = HttpContext.Current.Server.MapPath("~/App_Data");
            StringBuilder psPath = new StringBuilder(path).Append(@"\Add-NewMobilitiRoute.ps1");


            PowerShell ps = PowerShell.Create();
            //string directory = HttpContext.Current.Server.MapPath("~/App_Data/Add-NewMobilitiRoute.ps1");
            ps.AddCommand(psPath.ToString());
            ps.AddParameter("url", path);
            ps.AddParameter("Options", "ListAll");

            Collection<PSObject> results = ps.Invoke();
            Collection<ErrorRecord> errors = ps.Streams.Error.ReadAll();

            List<string> list = new List<string>();
            try
            {
                if (errors.Count > 0)
                {
                    foreach (var item in errors)
                    {
                        list.Add(item.Exception.Message);
                    }
                    msg = Request.CreateResponse(HttpStatusCode.BadRequest, list);

                }
                else
                {

                    foreach (PSObject item in results)
                    {
                        list.Add(item.Members["Value"].Value.ToString());
                    }

                    msg = Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (ActionPreferenceStopException e)
            {
                Object m = new { data = e.Message };
                msg = Request.CreateResponse(HttpStatusCode.BadRequest, m);
                
            }

            return msg;
        }


        // POST api/powershell
        public HttpResponseMessage Post([FromBody] MobilitiRoute value)
        {
            string path = HttpContext.Current.Server.MapPath("~/App_Data");
            StringBuilder psPath = new StringBuilder(path).Append(@"\Add-NewMobilitiRoute.ps1");

            PowerShell ps = PowerShell.Create();
            ps.AddCommand(psPath.ToString());
            ps.AddParameter("url", path);
            ps.AddParameter("acronym", value.acronym);
            ps.AddParameter("Options", "add");

            HttpResponseMessage message = new HttpResponseMessage();

            try
            {
                ps.Invoke();
                message = Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (ParameterBindingException e)
            {
                Object msg = new { data = e.Message };
                message = Request.CreateResponse(HttpStatusCode.BadRequest, msg);
            }
            catch (ActionPreferenceStopException e)
            {
                Object msg = new { data = e.Message };
                message = Request.CreateResponse(HttpStatusCode.BadRequest, msg);
            }


            return message;
        }

        // PUT api/powershell/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/powershell/5
        public HttpResponseMessage Delete(string id)
        {
            string path = HttpContext.Current.Server.MapPath("~/App_Data");
            StringBuilder psPath = new StringBuilder(path).Append(@"\Add-NewMobilitiRoute.ps1");

            PowerShell ps = PowerShell.Create();
            ps.AddCommand(psPath.ToString());
            ps.AddParameter("url", path);
            ps.AddParameter("acronym", id);
            ps.AddParameter("Options", "remove");

            HttpResponseMessage message = new HttpResponseMessage();

            try
            {
                ps.Invoke();
                message = Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (ParameterBindingException e)
            {
                Object msg = new { data = e.Message };
                message = Request.CreateResponse(HttpStatusCode.BadRequest, msg);
            }


            return message;
        }
    }

    public struct MobilitiRoute
    {
        public string acronym { get; set; }
        [System.ComponentModel.DefaultValue(false)]
        public bool isRemove { get; set; }

        public object config { get; set; }
    }
}
