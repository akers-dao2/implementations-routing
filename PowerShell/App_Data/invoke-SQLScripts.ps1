﻿<#
.Synopsis
   Short description
.DESCRIPTION
   Long description
.EXAMPLE
   Example of how to use this cmdlet
.EXAMPLE
   Another example of how to use this cmdlet
#>

[CmdletBinding()]
[OutputType([int])]
Param
(
    # Param1 help description
    [Parameter(
                ValueFromPipelineByPropertyName=$true,
                Position=0)]
    [int]
    $RoutingID = 11111111,

    # Param2 help description
    [string]
    $TranslatedRoutingID = "xxx40bank",
         [string]
    $GatewayServicePath = "",
        [string]
    $BusinessServicePath = "dnaweb.opensolutionsasp.com/DNAwebServices",
        [string]
    $DataServicePath = "dnaweb.opensolutionsasp.com/$TranslatedRoutingID",
    [switch]
    $isDelete,
    [string]
    $url
)

Begin
{
}
Process
{

    Import-Csv -Path "$url\SQLScripts.txt" | foreach -Process {Set-Variable -Name $_.key -Value $_.value}

    $instance = Get-ChildItem SQLSERVER:\SQL\FALOSAKERSF570\SQLEXPRESS\Databases

    if($isDelete){
        invoke-sqlcmd -ServerInstance FALOSAKERSF570\SQLEXPRESS -Database OSICENTRAL -Query "delete from RoutingIDTranslation where translatedroutingid = '$($TranslatedRoutingID)'"
        invoke-sqlcmd -ServerInstance FALOSAKERSF570\SQLEXPRESS -Database OSICENTRAL -Query "delete from ServiceLocation where routingid = '$($TranslatedRoutingID)'"
    } else{
        Invoke-Sqlcmd -ServerInstance FALOSAKERSF570\SQLEXPRESS -Database OSICENTRAL -Variable "RoutingID='$($RoutingID)'", "TranslatedRoutingID='$($TranslatedRoutingID)'","GatewayServicePath='$($GatewayServicePath)'", "BusinessServicePath='$($BusinessServicePath)'", "DataServicePath='$($DataServicePath)'"   -InputFile "C:\Users\fakers\Documents\Visual Studio 2013\Projects\PowerShell\PowerShell\App_Data\scripts.txt"

    }
}
End
{
}
