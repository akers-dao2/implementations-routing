##Implementations Routing
----------

The Implementations Routing application is a software program that create routing within the web.config file used for managing traffic for mobile banking application. In addition, runs several insert scripts to update database tables.

**Key Features:** CRUD, eliminates repetitive task, eliminates errors within web.config file, reduce the number of users needed access to SQL server, improve efficiency and user friendly

**Software Used:**

1. CSS3 (BootStrap http://getbootstrap.com)
1. HTML5 
1. Angularjs (https://angularjs.org) use for MVVM
1. PowerShell 4.0
1. .Net Web Api 2.0 

----------

**Example**
![implementation screen](impRouterScreen.png)